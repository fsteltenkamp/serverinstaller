#!/bin/bash
ARG=$1

if [ "$ARG" == "cleanup" ]; then
    echo "Cleaning up..."
    if [ -d "libs" ]; then
        rm -r "libs"
    fi
    if [ -d "installers" ]; then
        rm -r "installers"
    fi
    exit 1
fi

if [ ! -d "libs" ]; then
    mkdir "libs"
fi

if [ ! -f "libs/downloaders.sh" ]; then
    wget -qO "libs/downloaders.sh" https://gitlab.com/fsteltenkamp/bashlibraries/raw/master/downloaders.sh
    # shellcheck disable=SC1091
    source "libs/downloaders.sh"
else
    # shellcheck disable=SC1091
    source "libs/downloaders.sh"
fi
#include sysinfo lib first
getLib "sysinfo"
#includeLibraries
getLib "file" "http" "input" "output" "packages" "various"

#start
coloredEcho "================================================" cyan
coloredEcho "              ServerInstaller v 0.1             " cyan
coloredEcho "================================================" cyan
coloredEcho "Checking Privileges" cyan
if [ "$(id -u)" != "0" ]; then
    coloredEcho "This script must be run as root!" red
    exit 1
else
    coloredEcho "Privileges check out" green
fi
coloredEcho "Updating Packages..." cyan
packageUpdate

#=============================================================
#install single app or go through list?
#=============================================================
#change to -n it says use -z.. wtf
#shellcheck disable=SC2236
if [ ! -z "$1" ]; then
    #there was a parameter submitted!
    #get singleInstaller
    installer "$1"
else
    if prompt_confirm "Install basic Packages?"; then
        coloredEcho "Installing basic packages..." cyan
        packageInstall "mc nload htop ethstatus whowatch curl apt-transport-https screen sudo git ncdu coreutils make cmake gcc cifs-utils"
    fi

    if prompt_confirm "Do you want to add Users?"; then
        users=$( user_input "Please enter the names of the users (Comma separated): " | tr "," "\n")
        for user in $users; do
            adduser "$user"
        done
    fi

    if prompt_confirm "Install a PublicKey for SSH Authentication?"; then
        keyUrl=$(user_input "Please enter the URL of the Publickey: ")
        key=$(curl "$keyUrl")
        if prompt_confirm "Install in root?"; then
            checkCreateDir "/root/.ssh" && touch "/root/.ssh/authorized_keys" && echo "$key" >> "/root/.ssh/authorized_keys"
        fi
        if prompt_confirm "Install for any other user?"; then
            users=$( user_input "Please define users to add the key (comma separated list): " | tr "," "\n")
            for user in $users; do
                coloredEcho "adding key to $user" green
                checkCreateDir "/home/$user/.ssh" && touch "/home/$user/.ssh/authorized_keys" && echo "$key" >> "/home/$user/.ssh/authorized_keys"
            done
        fi
    fi

    if prompt_confirm "Add User to Sudo-List?"; then
        user=$(user_input "Please specify the username: ")
        adduser "$user" sudo
    fi

    if prompt_confirm "Install Webmin?"; then
        getInstaller "webmin"
        webminInstall
    fi

    if prompt_confirm "Install Usermin?"; then
        userminInstall
    fi

    if prompt_confirm "Install Webserver?"; then
        getInstaller "webserver"
        webserverInstall
    fi

    if prompt_confirm "Install MySQL Server?"; then
        getInstaller "mysql"
        mysqlInstall
    fi

    if prompt_confirm "Install Java Manually?"; then
        getInstaller "java"
        javaInstallMan
    fi

    if prompt_confirm "Install OpenJDK Java 8 Package?"; then
        getInstaller "java"
        javaInstallOpenJDK8
    fi

    if prompt_confirm "Install OpenJDK Java 7 Package?"; then
        getInstaller "java"
        javaInstallOpenJDK7
    fi

    if prompt_confirm "Install Server Motd?"; then
        getInstaller "motd"
        installmotd
    fi

    if prompt_confirm "Install .bashrc?"; then
        defaultbashrc="https://gitlab.com/snippets/1719594/raw"
        downloadFile $defaultbashrc "$HOME/.bashrc"
        coloredEcho ".bashrc has been installed." green
        if prompt_confirm "Install .bashrc for other (existing) users?"; then
            users=$( user_input "Please enter the names of the desired users (comma separated): " | tr "," "\n")
            for user in $users; do
                touch "/home/$user/.bashrc"
                echo "$defaultbashrc" > "/home/$user/.bashrc"
            done
        fi
        coloredEcho "You will have to copy it over to new users yourself." cyan
    fi

    if prompt_confirm "Install netdata?"; then
        getInstaller "netdata"
        netdataInstall
    fi

    if prompt_confirm "Install Munin-node?"; then
        getInstaller "munin"
        if prompt_confirm "Install Munin-Master aswell?"; then
            muninMasterInstall
        else
            muninNodeInstall
        fi
        
        if prompt_confirm "Configure (additional) Master-Node?"; then 
            muninNodeAddMaster
        fi
        
        if prompt_confirm "Install additional Munin-PLugins?"; then
            muninPluginsInstall
        fi
        
        if prompt_confirm "Configure plugins?"; then
            muninPluginsConfigure
        fi
    fi

    if prompt_confirm "Install .NET Core Runtime?"; then
        getInstaller "netcorert"
        netcorertInstall
    fi

    if prompt_confirm "Install Docker-CE?"; then
        getInstaller "docker"
        dockerInstall
    fi

    #if prompt_confirm "Install Elasticsearch?"; then
    #    getInstaller "elasticsearch"
    #    elasticsearchInstall
    #fi

    #if prompt_confirm "Install Elasticsearch-Kibana?"; then
    #    getInstaller "elasticsearch"
    #    kibanaInstall
    #fi

    #if prompt_confirm "Install Elasticsearch-Logstash?"; then
    #    getInstaller "elasticsearch"
    #    logstashInstall
    #fi

    #if prompt_confirm "Install Elasticsearch-Beats?"; then
    #    getInstaller "elasticsearchBeats"
    #    if prompt_confirm "Install Filebeat?"; then
    #        filebeatInstall
    #    fi
    #    if prompt_confirm "Install Packetbeat?"; then
    #        packetbeatInstall
    #    fi
    #    if prompt_confirm "Install Metricbeat?"; then
    #        metricbeatInstall
    #    fi
    #    if prompt_confirm "Install Heartbeat?"; then
    #        heartbeatInstall
    #    fi
    #    if prompt_confirm "Install Auditbeat?"; then
    #        auditbeatInstall
    #    fi
    #fi

    if prompt_confirm "Clean up?"; then
        $0 cleanup
        if prompt_confirm "Delete installer itself?"; then
            rm "$0"
            exit 1
        fi
    fi
fi